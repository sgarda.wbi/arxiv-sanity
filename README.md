# ArXiv sanity

My version of ArXiv sanity.

Auto-start script `run-arxiv-sanity.sh`.

I run this on my local machine and generate a RSS XML every day with recommendations that I read w/ newsboat.
In the feed there are backlinks to a running instance of `arxiv-sanity-lite` where I can do more tagging.

See `README_original.md` for the original readme of the project.
