"""
Compose and send recommendation emails to arxiv-sanity-lite users!

I run this script in a cron job to send out emails to the users with their
recommendations. There's a bit of copy paste code here but I expect that
the recommendations may become more complex in the future, so this is ok for now.

You'll notice that the file sendgrid_api_key.txt is not in the repo, you'd have
to manually register with sendgrid yourself, get an API key and put it in the file.
"""

import argparse
import time

import numpy as np
from loguru import logger
from lxml import etree as ET
# import pandas as pd
from sklearn import svm

from aslite.db import (get_email_db, get_metas_db, get_papers_db, get_tags_db,
                       load_features)

BASE_URL = "http://127.0.0.1:5000/"


def parse_args() -> argparse.Namespace:
    """
    Parse arguments
    """

    parser = argparse.ArgumentParser(description="Sends emails with recommendations")
    parser.add_argument(
        "--num-recommendations",
        type=int,
        default=25,
        help="number of recommendations to send per person",
    )
    parser.add_argument(
        "--time-delta",
        type=int,
        default=3,
        help="how recent papers to recommended, in days",
    )
    parser.add_argument(
        "--dry-run",
        type=int,
        default=0,
        help="if set to 1 do not actually send the emails",
    )
    parser.add_argument(
        "--user",
        type=str,
        default="",
        help="restrict recommendations only to a single given user (used for debugging)",
    )
    parser.add_argument(
        "--min-papers",
        type=int,
        default=1,
        help="user must have at least this many papers for us to send recommendations",
    )
    parser.add_argument(
        "--out",
        type=str,
        default="./feed/rss.xml",
        help="Where to store RSS XML file w/ recommendations",
    )
    args = parser.parse_args()

    return args


def calculate_recommendation(
    tags,
    time_delta=3,  # how recent papers are we recommending? in days
):
    """
    Compute recommendations
    """

    # read entire db simply into RAM
    with get_metas_db() as mdb:
        metas = dict(mdb.items())

    tnow = time.time()

    # read tfidf features into RAM
    features = load_features()

    # a bit of preprocessing
    x, pids = features["x"], features["pids"]
    n, _ = x.shape
    ptoi, itop = {}, {}
    for i, p in enumerate(pids):
        ptoi[p] = i
        itop[i] = p

    # loop over all the tags
    all_pids, all_scores = {}, {}
    for tag, pids in tags.items():

        if len(pids) == 0:
            continue

        # construct the positive set for this tag
        y = np.zeros(n, dtype=np.float32)
        for pid in pids:
            y[ptoi[pid]] = 1.0

        # classify
        clf = svm.LinearSVC(
            class_weight="balanced", verbose=False, max_iter=10000, tol=1e-6, C=0.01
        )
        clf.fit(x, y)
        s = clf.decision_function(x)
        sortix = np.argsort(-s)
        pids = [itop[ix] for ix in sortix]
        scores = [100 * float(s[ix]) for ix in sortix]

        # filter by time to only recent papers
        deltat = time_delta * 60 * 60 * 24  # allowed time delta in seconds
        keep = [
            i for i, pid in enumerate(pids) if (tnow - metas[pid]["_time"]) < deltat
        ]
        pids, scores = [pids[i] for i in keep], [scores[i] for i in keep]

        # finally exclude the papers we already have tagged
        have = set().union(*tags.values())
        keep = [i for i, pid in enumerate(pids) if pid not in have]
        pids, scores = [pids[i] for i in keep], [scores[i] for i in keep]

        # store results
        all_pids[tag] = pids
        all_scores[tag] = scores

    return all_pids, all_scores


# -----------------------------------------------------------------------------


def write_xml(pids, scores, max_source_tag, num_recommendations, out):
    """
    Write reccomendations as RSS XML
    """

    # keep the papers as only a handle, since this can be larger
    pdb = get_papers_db()

    root = ET.Element("rss")
    root.set("version", "2.0")
    channel = ET.SubElement(root, "channel")

    n = min(len(scores), num_recommendations)
    for pid_score, pid in zip(scores[:n], pids[:n]):

        p = pdb[pid]

        # create the url that will feature this paper on top and also show the most similar papers
        item = ET.SubElement(channel, "item")

        title = ET.SubElement(item, "title")
        title.text = (
            p["title"] + f"[tag:{max_source_tag[pid]}, score:{round(pid_score, 4)}]"
        )

        link = ET.SubElement(item, "link")
        link.text = f"{BASE_URL}?rank=pid&pid=" + pid

        author = ET.SubElement(item, "author")
        author.text = ", ".join(a["name"] for a in p["authors"])

        description = ET.SubElement(item, "description")
        description.text = "<p>" + p["summary"] + "</p>"

    tree = ET.ElementTree(root)
    tree.write(out, pretty_print=True, xml_declaration=True)


def save_recommendations(tag_pids, tag_scores, num_recommendations, out):
    """
    render the paper recommendations into the html template
    """
    # first we are going to merge all of the papers / scores together using a MAX
    max_score: dict = {}
    max_source_tag = {}
    for tag in tag_pids:
        for pid, score in zip(tag_pids[tag], tag_scores[tag]):
            max_score[pid] = max(max_score.get(pid, -99999), score)  # lol
            if max_score[pid] == score:
                max_source_tag[pid] = tag

    # now we have a dict of pid -> max score. sort by score
    max_score_list = sorted(max_score.items(), key=lambda x: x[1], reverse=True)
    merged_pids, merged_scores = zip(*max_score_list)

    write_xml(
        pids=merged_pids,
        scores=merged_scores,
        max_source_tag=max_source_tag,
        num_recommendations=num_recommendations,
        out=out,
    )


def main():
    """
    Run script
    """
    args = parse_args()

    # read entire db simply into RAM
    with get_tags_db() as tags_db:
        tags = dict(tags_db.items())

    # read entire db simply into RAM
    with get_email_db() as edb:
        emails = dict(edb.items())

    # iterate all users, create recommendations, send emails
    # num_sent = 0
    for user, user_tags in tags.items():

        # verify that we have an email for this user
        email = emails.get(user, None)
        if not email:
            logger.info("skipping user {}, no email", user)
            continue
        if args.user and user != args.user:
            logger.info("skipping user {}, not {}", user, args.user)
            continue

        # verify that we have at least one positive example...
        num_papers_tagged = len(set().union(*user_tags.values()))
        if num_papers_tagged < args.min_papers:
            logger.info(
                "skipping user {}, only has {} papers tagged", user, num_papers_tagged
            )
            continue

        # insert a fake entry in tags for the special "all" tag, which is the union of all papers
        # tags['all'] = set().union(*tags.values())

        pids, scores = calculate_recommendation(
            tags=user_tags, time_delta=args.time_delta
        )
        if all(len(lst) == 0 for tag, lst in pids.items()):
            logger.info("skipping user {}, no recommendations were produced", user)
            continue

        logger.info(
            "saving top {} recommendations into a report for {}...",
            args.num_recommendations,
            user,
        )
        save_recommendations(
            tag_pids=pids,
            tag_scores=scores,
            num_recommendations=args.num_recommendations,
            out=args.out,
        )


if __name__ == "__main__":
    main()
