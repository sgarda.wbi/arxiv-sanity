feedparser==6.0.8
Flask==2.0.2
scikit-learn==1.2.1
sqlitedict==1.7.0
loguru==0.6.0
lxml==4.9.1
