"""
This script is intended to wake up every 30 min or so (eg via cron),
it checks for any new arxiv papers via the arxiv API and stashes
them into a sqlite database.
"""

import argparse
import os
import random
import sys
import time

from loguru import logger

from aslite.arxiv import get_response, parse_response
from aslite.db import DATA_DIR, get_metas_db, get_papers_db


def parse_args() -> argparse.Namespace:
    """
    Quick note on the break_after argument: In a typical setting where one wants to update
    the papers database you'd choose a slightly higher num, but then break out early in case
    we've reached older papers that are already part of the database, to spare the arxiv API.
    """

    parser = argparse.ArgumentParser(description="Arxiv Daemon")
    parser.add_argument(
        "-n", "--num", type=int, default=100, help="up to how many papers to fetch"
    )
    parser.add_argument(
        "-s", "--start", type=int, default=0, help="start at what index"
    )
    parser.add_argument(
        "-b",
        "--break-after",
        type=int,
        default=3,
        help="how many 0 new papers in a row would cause us to stop early? or 0 to disable.",
    )
    args = parser.parse_args()

    return args


def main():
    """
    Run deamon
    """

    logger.remove()
    logger.add(os.path.join(DATA_DIR, "logs", "fetch.log"), rotation="1 week")

    args = parse_args()

    # query string of papers to look for
    # q = 'cat:cs.CV+OR+cat:cs.LG+OR+cat:cs.CL+OR+cat:cs.AI+OR+cat:cs.NE+OR+cat:cs.RO'
    # q = "cat:cs.CL+OR+cat:cs.LG+OR+cat:cs.IR"
    q = "cat:cs.CL+OR+cat:cs.IR"
    pdb = get_papers_db(flag="c")
    mdb = get_metas_db(flag="c")
    prevn = len(pdb)

    def store(p):
        pdb[p["_id"]] = p
        mdb[p["_id"]] = {"_time": p["_time"]}

    # fetch the latest papers
    total_updated = 0
    zero_updates_in_a_row = 0
    for k in range(args.start, args.start + args.num, 100):
        logger.info("querying arxiv api for query {} at start_index {}", q, k)

        # attempt to fetch a batch of papers from arxiv api
        ntried = 0
        while True:
            try:
                resp = get_response(search_query=q, start_index=k)
                papers = parse_response(resp)
                time.sleep(0.5)
                if len(papers) == 100:
                    break  # otherwise we have to try again
            except Exception as e:
                logger.warning(e)
                logger.warning("will try again in a bit...")
                ntried += 1
                if ntried > 1000:
                    logger.error(
                        "ok we tried 1,000 times, something is srsly wrong. exitting."
                    )
                    sys.exit()
                time.sleep(2 + random.uniform(0, 4))

        # process the batch of retrieved papers
        nhad, nnew, nreplace = 0, 0, 0
        for p in papers:
            pid = p["_id"]
            if pid in pdb:
                if p["_time"] > pdb[pid]["_time"]:
                    # replace, this one is newer
                    store(p)
                    nreplace += 1
                else:
                    # we already had this paper, nothing to do
                    nhad += 1
            else:
                # new, simple store into database
                store(p)
                nnew += 1
        prevn = len(pdb)
        total_updated += nreplace + nnew

        # some diagnostic information on how things are coming along
        logger.info(papers[0]["_time_str"])
        logger.info(
            "k={}, out of {}: had {}, replaced {}, new {}. now have: {}",
            k,
            len(papers),
            nhad,
            nreplace,
            nnew,
            prevn,
        )

        # early termination criteria
        if nnew == 0:
            zero_updates_in_a_row += 1
            if args.break_after > 0 and zero_updates_in_a_row >= args.break_after:
                logger.info(
                    "breaking out early, no new papers {} times in a row",
                    args.break_after,
                )
                break
            elif k == 0:
                logger.info(
                    "our very first call for the latest there were no new papers, exitting"
                )
                break
        else:
            zero_updates_in_a_row = 0

        # zzz
        time.sleep(1 + random.uniform(0, 3))

    # exit with OK status if anything at all changed, but if nothing happened then raise 1
    sys.exit(0 if total_updated > 0 else 1)


if __name__ == "__main__":
    main()
